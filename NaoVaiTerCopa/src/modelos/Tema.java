
package modelos;

public class Tema {
    
    private String saude;
    private String alimentacao;
    private String tranasporte;
    private String energia;
    private String educacao;
    private String outrosTemas;
    
    public Tema(String saude, String alimentacao, String transporte, String energia, String educacao, String outrosTemas){
            this.saude = saude;
            this.alimentacao = alimentacao;
            this.tranasporte = transporte;
            this.energia = energia;
            this.educacao = educacao;
            this.outrosTemas = outrosTemas;
    }

	public String getSaude() {
		return saude;
	}

	public void setSaude(String saude) {
		this.saude = saude;
	}

	public String getAlimentacao() {
		return alimentacao;
	}

	public void setAlimentacao(String alimentacao) {
		this.alimentacao = alimentacao;
	}

	public String getTranasporte() {
		return tranasporte;
	}

	public void setTranasporte(String tranasporte) {
		this.tranasporte = tranasporte;
	}

	public String getEnergia() {
		return energia;
	}

	public void setEnergia(String energia) {
		this.energia = energia;
	}

	public String getEducacao() {
		return educacao;
	}

	public void setEducacao(String educacao) {
		this.educacao = educacao;
	}

	public String getOutrosTemas() {
		return outrosTemas;
	}

	public void setOutrosTemas(String outrosTemas) {
		this.outrosTemas = outrosTemas;
	}
    
}
