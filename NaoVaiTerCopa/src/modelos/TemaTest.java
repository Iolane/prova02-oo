package modelos;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TemaTest {
	
	private Tema tema;

	@Before
	public void setUp() throws Exception {
		
		tema = new Tema();
		
	}

	@Test
	public void testTema() {
		
		
	}

	@Test
	public void testGetSaude() {
		String teste = "XXXX";
		tema.setSaude(teste);
		assertEquals(teste, tema.getSaude());
	}


	@Test
	public void testGetAlimentacao() {
		String teste = "XXXX";
		tema.setAlimentacao(teste);
		assertEquals(teste, tema.getAlimentacao());
		
	}


	@Test
	public void testGetTranasporte() {
		String teste = "XXXX";
		tema.setTransporte(teste);
		assertEquals(teste, tema.getTransporte());
	}
	}


	@Test
	public void testGetEnergia() {
		String teste = "XXXX";
		tema.setEnergia(teste);
		assertEquals(teste, tema.getEnergia());
	}
		
	}


	@Test
	public void testGetEducacao() {
		String teste = "XXXX";
		tema.setEducacao(teste);
		assertEquals(teste, tema.getEducacao());
			
	}

	@Test
	public void testGetOutrosTemas() {
		String teste = "XXXX";
		tema.setSaude(teste);
		assertEquals(teste, tema.getSaude());
	}


}
