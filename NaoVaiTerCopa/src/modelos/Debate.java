package modelos;

public class Debate {
    
    private String titulo;
    private String descricao;
    private Tema tema;
    
    public Debate(String titulo, String descricao, Tema tema){
    	this.titulo = titulo;
    	this.descricao = descricao;
    	this.tema = tema;
    }
	
    
    public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Tema getTema() {
		return tema;
	}
	public void setTema(Tema tema) {
		this.tema = tema;
	}
    
    
}
