package modelos;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestDebate {
	
	private Debate debate = new Debate();

	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void testTitulo() {
		String mensagem = "XXXXX";
		debate.setTitulo(mensagem);
		assertEquals(mensagem, debate.getTitulo());
		
	}
	
	@Test
	public void testDescricao() {
		String mensagem = "XXXXX";
		debate.setDescricao(mensagem);
		assertEquals(mensagem, debate.getDescricao());
		
	}
	
	@Test
	public void testTema() {
		String mensagem = "XXXXX";
		debate.setTema(mensagem);
		assertEquals(mensagem, debate.getTema());
		
	}
	

	
	
	

}
